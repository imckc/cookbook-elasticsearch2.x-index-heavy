script "add_esusers" do
  interpreter "bash"
  user "root"
  cwd "/tmp"
  code <<-EOH
    #{node[:elasticsearch][:home_dir]}/bin/shield/esusers useradd kibana -r admin -p password
    #{node[:elasticsearch][:home_dir]}/bin/shield/esusers useradd el@stics3arch -r admin -p 'aioshp!r8uf@ae3'
    EOH
end
