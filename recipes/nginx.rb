script "install_nginx" do
  interpreter "bash"
  user "root"
  cwd "/tmp"
  code <<-EOH
    yum -y install nginx
    /etc/init.d/nginx start
  EOH
end

# services
execute "start-nginx" do
  command "nginx start"
  action :nothing
end